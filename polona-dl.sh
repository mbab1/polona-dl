#!/usr/bin/env bash
# version 0.0.1

[ -z "$1" ] && echo "No url or id given" && exit

ID="$1"
ID=${ID##*item/}
ID=${ID##*,}
ID=${ID%%/*}

T=$(mktemp)

curl -s "https://polona.pl/api/entities/${ID}/" > "$T"

PDF=$(jq -r '.resources[] | select(.mime=="application/pdf") | .url' < "$T")
TITLE=$(jq -r '.title' < "$T")
EXT="pdf"

if [ -n "$PDF" ]; then
  echo -en "\rDownload PDF file"
  wget -q -O "${TITLE:0:77}.pdf" "$PDF"
else
  TD=$(mktemp -d)
  N=0
  TP=$(jq -r '.scans[].resources[] | select(.mime=="image/jpeg")' < "$T" | jq -s '. | length')
  jq -r '.scans[].resources[] | select(.mime=="image/jpeg") | .url' < "$T" | while read LINE; do
    N=$((N+1))
    echo -en "\rDownload page $N/$TP"
    wget -q -O $(printf "$TD/%03d.jpg" $N) "$LINE"
  done
  echo -en "\rPacking $N pages"
  P=$(pwd)
  cd "$TD"
  zip -q "$P/$TITLE.cbz" *
  cd - > /dev/null
  rm -rf "$TD"
  EXT=".cbz"
fi

rm $T

echo -e "\rFile $TITLE.$EXT ready"
